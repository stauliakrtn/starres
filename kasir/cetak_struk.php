<?php
include '../koneksi.php';
require('../admin/fpdf/fpdf.php');

$pdf = new FPDF("P","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('../logo.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'Star Resto',0,'P');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 083811584860',0,'P');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jl. Mangga',0,'P');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : starresto.sknc.site : staulkrtn21@gmail.com',0,'P');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(15.5,0.7,"Struk Pembayaran",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$get_id = $_GET['id_order'];
$query2 = mysqli_query($conn, "SELECT * FROM transaksi INNER JOIN user ON transaksi.id_user = user.id_user WHERE transaksi.id_order = '$get_id'");
$lihat4 = mysqli_fetch_array($query2);
$pdf->Cell(5,0.7,"Tanggal	: ".date("D-d/m/Y"),0,0,'C');
$pdf->Cell(17.5,0.7,"Kasir	: ".$lihat4['nama_user'],0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'No', 1, 0, 'C');
$pdf->Cell(5.5, 0.8, 'Nama Menu', 1, 0, 'C');
$pdf->Cell(4.5, 0.8, 'Harga', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Quantity', 1, 0, 'C');
$pdf->Cell(3.5, 0.8, 'Total Harga', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$id_get_order_id = $_GET['id_order'];
$query = mysqli_query($conn, "SELECT * FROM oder INNER JOIN detail_order ON oder.id_order = detail_order.id_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan INNER JOIN transaksi ON oder.id_order = transaksi.id_order WHERE oder.id_order = '$id_get_order_id'");
while($r = mysqli_fetch_array($query)){
$jumlah=$r['jumlah']*$r['harga'];
$harga=$r['harga'];
$hasil="Rp.".number_format($harga,2,',','.');
$hasil_jumlah="Rp.".number_format($jumlah,2,',','.');

	$pdf->Cell(1, 0.6, $no , 1, 0, 'C');
	$pdf->Cell(5.5, 0.6, $r['nama_masakan'],1, 0, 'C');
	$pdf->Cell(4.5, 0.6, $hasil, 1, 0,'C');
	$pdf->Cell(2, 0.6, $r['jumlah'],1, 0, 'C');
	$pdf->Cell(3.5, 0.6, $hasil_jumlah,1, 1, 'C');

	$no++;
}
$query1 = mysqli_query($conn, "SELECT * FROM transaksi WHERE id_order= '$id_get_order_id'");
$total_bayar = mysqli_fetch_array($query1);
   $harga=$total_bayar['total_bayar'];
   $hasil="Rp.".number_format($harga,2,',','.');
   $harga1=$total_bayar['jumlah_uang'];
   $jumlah_uang="Rp.".number_format($harga1,2,',','.');
   $harga2 = $total_bayar['kembalian'];
   $kembalian = "Rp".number_format($harga2,2,',','.');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->SetX(8.65);
$pdf->Cell(15.5, 0.9, "Total 				:			".$hasil, 0, 0, 'C');

$pdf->ln(1);
$pdf->SetX(8.65);
$pdf->Cell(17, 0.7, "Jumlah Uang 			:				".$jumlah_uang, 0, 0, 'C');

$pdf->ln(1);
$pdf->SetX(8.65);
$pdf->Cell(16.5, 0.7, "Kembalian 			:				".$kembalian, 0, 0, 'C');

$pdf->Output("struk_pembayaran.pdf","I");

?>

