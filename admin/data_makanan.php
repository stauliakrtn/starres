 <?php
 include 'head.php';
 include'../database.php';
$db = new database();
 ?>
 <div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
      <div class="page-title">
        <h1>Dashboard</h1>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="#">Dashboard</a></li>
          <li><a href="#">Table</a></li>
          <li class="active">Data table</li>
        </ol>
      </div>
    </div>
  </div>
</div>
 <div class="content mt-3">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Table Data Orderan</strong>
          </div>
          <div class="card-body">
            <a href="laporan_datamak.php" class="btn btn-success"><i class="fa fa-print"></i>&nbsp; Print</a>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahmasakan"><i class=""></i>&nbsp;+ Tambah Data</button>
                        <br><br>
          
                  <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Masakan</th>
                        <th>Harga</th>
                        <th>Gambar</th>
                        <th>Status Masakan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach($db->tampil_data_masakan() as $x){
                      ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $x['nama_masakan']; ?></td>
                        <td><?php echo $x['harga']; ?></td>
                        <td><img src="../images/<?php echo $x['gambar']; ?>" height='100'></td>
                        <td>
                          <?php
                          if($x['status_makanan'] == 'Y')
                          {
                          ?>
                          <a href="approve_masakan.php?table=masakan&id_masakan=<?php echo $x['id_masakan']; ?>&action=not-verifed" class="btn btn-info btn-md">
                          Tersedia
                          </a>
                          <?php
                          }else{
                          ?>
                          <a href="approve_masakan.php?table=masakan&id_masakan=<?php echo $x['id_masakan']; ?>&action=verifed" class="btn btn-danger btn-md">
                          Habis
                          </a>
                          <?php
                          }
                          ?>
                        </td>
                        <td>
                        <a href="" data-toggle="modal" data-target="#editmasakan<?php echo $x['id_masakan'];?>" class="btn btn-primary">Edit</a>      
                        </td>
                      </tr>
            <div class="modal" id="editmasakan<?php echo $x['id_masakan'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Form Edit Menu Makanan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <?php
                  include '../koneksi.php';
                  $id = $x['id_masakan']; 
                  $query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id'");
                  $r = mysqli_fetch_array($query_edit);
                  ?>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="update_masakan.php?id_masakan=<?php echo $r['id_masakan'];?>" enctype="multipart/form-data">
                      <div class="col-md-6">
                            <div class="form-group">
                              <label for="nama_masakan">Nama Masakan :</label>
                              <input type="hidden" name="id_masakan" value="<?php echo $r['id_masakan'];?>">
                              <input type="text" class="form-control" name="nama_masakan" id="nama_masakan" value="<?php echo $r['nama_masakan'];?>" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                              <label for="harga">Harga :</label>
                              <input type="integer" class="form-control" name="harga" id="harga" value="<?php echo $r['harga'];?>" placeholder="Masukan Harga">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="Gambar">Masukan Gambar :</label>
                              <input type="file" name="gambar" value="../images/<?php echo $r['gambar'];?>">
                            </div>
                            <div class="form-group">
                            <label>Status Masakan</label>
                            <div class="checkbox">
                              <label><input type="checkbox" name="status_makanan" id="optionsCheckbox1" value="Y" checked> Tersedia</label>
                            </div>
                            <div class="checkbox">
                             <label><input type="checkbox"  name="status_makanan" id="optionsCheckbox2" value="N"> Habis</label>
                           </div>
                         </div>
                          </div>
                                
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                      
<?php } ?>
                    </tbody>
                  </table>
        
      <div class="modal" id="tambahmasakan">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Form Tambah Menu Masakan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                 <div class="modal-body">
                     <form role="form" method="POST" action="save_masakan.php" enctype="multipart/form-data">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Nama Masakan</label>
                          <input type="text" class="form-control" name="nama_masakan" id="exampleInputPassword1" placeholder="Masukan Nama Masakan">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Harga</label>
                          <input type="integer" class="form-control" name="harga" id="exampleInputPassword1" placeholder="Masukan Harga">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputFile">Masukan Gambar</label>
                          <input type="file" name="gambar">
                        </div>
                        <div class="form-group">
                          <label>Status Masakan</label>
                          <div class="checkbox">
                            <label><input type="checkbox" name="status_makanan" id="optionsCheckbox1" value="Tersedia" checked> Tersedia</label>
                          </div>
                          <div class="checkbox">
                           <label><input type="checkbox"  name="status_makanan" id="optionsCheckbox2" value="Habis"> Habis</label>
                         </div>
                       </div>
                     </div>
                     <!-- /.box-body -->
                     <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div><!-- /.box-body -->
                  </form>
                </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
include 'foot.php';
?>