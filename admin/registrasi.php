
<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Table</a></li>
                    <li class="active">Registrasi Meja</li>
                </ol>
            </div>
        </div>
    </div>
</div>
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form role="form"  method="POST" action="proses_hak.php?aksi=input_user" enctype="multipart/form-data" class="form-horizontal form-material"">
                                <div class="card-body card-block">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="exampleInputPassword1">Username</label>
                                          <input type="text" class="form-control" name="username" id="username" placeholder="Masukkan Username" required="Harus Diisi">
                                        </div>
                                        <div class="form-group">
                                          <label for="exampleInputPassword1">Email</label>
                                          <input type="text" class="form-control" name="email" id="email" required="Harus Diisi">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="exampleInputPassword1">Password</label>
                                          <input type="password" class="form-control" name="password" id="password" required="Harus Diisi">
                                        </div>
                                        <div class="form-group">
                                          <label for="exampleInputPassword1">Nama User</label>
                                          <input type="text" class="form-control" name="nama_user" id="nama_user" placeholder="Masukkan Nama" required="Harus Diisi">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Nama Level</label>
                                                <select name="id_level" class="form-control">
                                                    <option>Pilih Level</option>
                                                    <?php
                                                    include "../koneksi.php";
                                                    $select = mysqli_query($conn, "SELECT * FROM level");
                                                    while($data = mysqli_fetch_array($select)){
                                                    ?>
                                                    <option value="<?php echo $data['id_level'];?>"><?php echo $data['nama_level'];?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                             


<?php
include "foot.php";
?>