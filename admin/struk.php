<?php
include 'head.php';
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
      <div class="page-title">
        <h1>Dashboard</h1>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="#">Dashboard</a></li>
          <li><a href="#">Table</a></li>
          <li class="active">Data table</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
              
                <div class="col-lg-12">
                        <div class="card">
       <?php
          include '../koneksi.php';
          $id_get_order = $_GET['id_order'];
          $query = mysqli_query($conn,"SELECT id_order FROM oder WHERE id_order = '$id_get_order'");
          $r = mysqli_fetch_array($query);
          $id_order = $r['id_order'];
          ?>
                          <div class="card-body">
                            <a href="detail_transaksi.php" class="btn btn-danger"> Kembali</a>
                            
                            <a href="cetak_struk.php?id_order=<?php echo $id_order;?>" class="btn btn-success"> Cetak Struk</a>
                            <br><br>
                            <table id="" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Nama Masakan</th>
                                      <th>Harga</th>
                                      <th>Quantity</th>
                                      <th>Harga</th>
                                    </tr>
                                </thead>
                        <?php
                        $no = 1;
                        $id_get_order_id = $_GET['id_order'];
                        $query = mysqli_query($conn, "SELECT * FROM oder INNER JOIN detail_order ON oder.id_order = detail_order.id_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan INNER JOIN transaksi ON oder.id_order = transaksi.id_order WHERE oder.id_order = '$id_get_order_id'");
                        while($r = mysqli_fetch_array($query)){
                         $jumlah=$r['jumlah']*$r['harga'];
                         $harga=$r['harga'];
                         $hasil="Rp.".number_format($harga,2,',','.');
                         $hasil_kali="Rp.".number_format($jumlah,2,',','.');
                         $total_jumlah = $r['total_bayar'];
                         $total_jumlah_bayar = "Rp".number_format($total_jumlah,2,',','.');
                         $jumlah_uang = $r['jumlah_uang'];
                         $jumlah_uang_format = "Rp".number_format($jumlah_uang,2,',','.');
                         $kembalian = $r['kembalian'];
                         $kembalian_format = "Rp".number_format($kembalian,2,',','.');
                         ?>
                         <tbody>
                          
                          <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $r['nama_masakan']; ?></td>
                            <td><?php echo $hasil; ?></td>
                            <td><?php echo $r['jumlah']; ?></td>
                            <td><?php echo $hasil_kali;?></td>
                          </tr>
                        
                        <?php  } ?>
                      <tr>
                        <td colspan="4" align="right"><h4><b>Total Pembayaran</b></h4></td>
                        <td><h4><?php echo $total_jumlah_bayar;?></h4></td>
                      </tr>
                      <tr>
                        <td colspan="4" align="right"><h4><b>Jumlah Uang</b></h4></td>
                        <td><h4><?php echo $jumlah_uang_format;?></h4></td>
                      </tr>
                      <tr>
                        <td colspan="4" align="right"><h4><b>Uang Kembalian</b></h4></td>
                        <td><h4><?php echo $kembalian_format;?></h4></td>
                      </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
<?php
include 'foot.php';
?>