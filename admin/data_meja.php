 <?php
 include 'head.php';
 include'../database.php';
$db = new database();
 ?>
 <div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
      <div class="page-title">
        <h1>Dashboard</h1>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="#">Dashboard</a></li>
          <li><a href="#">Table</a></li>
          <li class="active">Data table</li>
        </ol>
      </div>
    </div>
  </div>
</div>
 <div class="content mt-3">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Table Data Meja</strong>
          </div>
          <div class="card-body">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahmasakan"><i class=""></i>&nbsp;+ Tambah Data</button>
                        <br><br>
          
                  <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Meja</th>
                        <th>Status Meja</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach($db->tampil_data_meja() as $x){
                      ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $x['no_meja']; ?></td>
                        <td>
                                <?php
                                if($x['status_meja'] == 'kosong')
                                {
                                ?>
                                <a href="approve_meja.php?table=meja&id_meja=<?php echo $x['id_meja']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                Kosong
                                </a>
                                <?php
                                }else{
                                ?>
                                <a href="approve_meja.php?table=meja&id_meja=<?php echo $x['id_meja']; ?>&action=verifed" class="btn btn-danger btn-md">
                                Penuh
                                </a>
                                <?php
                                }
                                ?>
                              </td>
                        <td>
                        <a href="" data-toggle="modal" data-target="#editmeja<?php echo $x['id_meja'];?>" class="btn btn-primary">Edit</a>      
                        </td>
                      </tr>
            <div class="modal" id="editmeja<?php echo $x['id_meja'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Form Edit Meja</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <?php
                  include '../koneksi.php';
                  $id = $x['id_meja']; 
                  $query_edit = mysqli_query($conn,"SELECT * FROM meja WHERE id_meja='$id'");
                  $r = mysqli_fetch_array($query_edit);
                  ?>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses_meja.php?aksi=update_meja" enctype="multipart/form-data">
                      <div class="col-md-6">
                            <div class="form-group">
                              <label for="no_meja">No Meja :</label>
                              <input type="hidden" name="id_meja" value="<?php echo $r['id_meja'];?>">
                              <input type="text" class="form-control" name="no_meja" id="no_meja" value="<?php echo $r['no_meja'];?>" placeholder="Masukan Nama">
                            </div>
                                
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                      
<?php } ?>
                    </tbody>
                  </table>
        
      <div class="modal" id="tambahmasakan">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Form Tambah Meja</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                 <div class="modal-body">
                     <form role="form" method="POST" action="save_meja.php" enctype="multipart/form-data">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="exampleInputPassword1">No Meja</label>
                          <input type="text" class="form-control" name="no_meja" id="exampleInputPassword1" placeholder="Masukan No Meja" required="Isikan No meja">
                        </div>
                      </div>
                     <!-- /.box-body -->
                     <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div><!-- /.box-body -->
                  </form>
                </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
include 'foot.php';
?>