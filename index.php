<?php
	include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Star Resto</title>
	
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Cafe In Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->
	
	<!-- css files -->
	<link rel="stylesheet" href="pelanggan/css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="pelanggan/css/style.css" type="text/css" media="all" /> <!-- Style-CSS --> 
	<link rel="stylesheet" href="pelanggan/css/fontawesome-all.css"> <!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	
	<link href="pelanggan/css/prettyPhoto.css" rel="stylesheet" type="text/css" />

	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Alegreya+Sans:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<!-- //web-fonts -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
</head>

<body>

<!-- Navigation -->
<header>
	<div class="top-nav">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand text-uppercase" href="index.php">Star Resto</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse justify-content-center pr-md-4" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="index.php">Menu <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#"><img  " src="images/logoo.png"></a>
                                <span class="">
                                    <?php
					                if(isset($_SESSION['items'])){
					                  echo count($_SESSION['items']);
					                }
					                else{
					                  echo "";
					                }
					                ?>
        						</span>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="login.php">Login</a>
						</li>
						
						<!-- search --->
						<div class="search-bar-agileits">
							
								<!-- cd-header-buttons -->
							</div>
						</div>
						<!-- search --->
				
					</ul>
					
				</div>

			</nav>
		</div>
	</div>
</header>
<!-- //Navigation -->

<!-- //About us -->

<div class="clearfix"></div>
<br><br><br>
<!-- food gallery -->
<section class="banner_bottom proj py-5">
		<div class="wrap_view">
		<h3 class="heading text-center text-uppercase mb-5"> DAFTAR MENU </h3>
			<div class="inner_sec">


				<ul class="portfolio-area">
					<?php
                          include'koneksi.php';
                                            // Include / load file koneksi.php
                                            // Cek apakah terdapat data pada page URL
                          $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                          $limit = 10; // Jumlah data per halamanya

                                            // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
                          $limit_start = ($page - 1) * $limit;

                                            // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                          $data=mysqli_query($conn, "SELECT * FROM masakan where status_makanan='Y' LIMIT ".$limit_start.",".$limit);
                          $no = $limit_start + 1; // Untuk penomoran tabel
                          while($rs=mysqli_fetch_array($data)){
                          	$harga = $rs['harga'];
					  		$hasil = "Rp".number_format($harga,2,',','.');
                        ?>
					<li class="portfolio-item2 mb-2" data-id="id-0" data-type="cat-item-4">
						<div>
							
							<span class="image-block img-hover">
							
							<a class="image-zoom" href="images/<?php echo $rs['gambar']; ?> " height='180' width="50" rel="prettyPhoto[gallery]">
							
									<img src="images/<?php echo $rs['gambar']; ?>" height='180' width="50" class="img-responsive" alt="Cafe In image">
									<div class="port-info">
											<h5><?php echo $rs['nama_masakan']; ?></h5>
											<p><?php echo $hasil; ?></p>
										</div>
							</a>
							
						</span>
						
						</div>
					</li>
						<?php } ?>
				</ul>
				<!--end portfolio-area -->

					<div class="clearfix"></div>
					<div class="pagination">
               <?php
            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
            ?>
                <li class="disabled"><a href="#">First</a></li>
                <li class="disabled"><a href="#">&laquo;</a></li>
            <?php
            } else { // Jika buka page ke 1
                $link_prev = ($page > 1) ? $page - 1 : 1;
            ?>
                <li><a href="index.php?page=1">First</a></li>
                <li><a href="index.php?page=<?php echo $link_prev; ?>">&laquo;</a></li>
            <?php
            }
            ?>

            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $sql2 = mysqli_query($conn,"SELECT COUNT(*) AS jumlah FROM masakan where status_makanan='Y' ");
            ($get_jumlah = (mysqli_fetch_array($sql2)));

            $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <li <?php echo $link_active; ?>><a href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
            }
            ?>

            <!-- LINK NEXT AND LAST -->
            <?php
            // Jika page sama dengan jumlah page, maka disable link NEXT nya
            // Artinya page tersebut adalah page terakhir
            if ($page == $jumlah_page) { // Jika page terakhir
            ?>
                <li class="disabled"><a href="#">&raquo;</a></li>
                <li class="disabled"><a href="#">Last</a></li>
            <?php
            } else { // Jika bukan page terakhir
                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
            ?>
                <li><a href="index.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
                <li><a href="index.php?page=<?php echo $jumlah_page; ?>">Last</a></li>
            <?php
            }
            ?>
             </div>
			</div>

		</div>
</section>
<!--//food gallery-->

<!-- footer -->	
<footer>
	<div class="container py-3 py-md-4">
		<div class="row">
			<div class="col-lg-5 col-md-12">
				<p class="py-lg-4">© 2019 UjikomTableService | Design by <a href="http://www.W3Layouts.com" target="_blank">W3Layouts</a></p>
			</div>
			<div class="col-lg-8 col-md-12">
				<a href="index.php">Star Resto</a>
			</div>
	</div>
</footer>
<!-- footer -->



<!-- js-scripts -->		

	<!-- js -->
	<script type="text/javascript" src="pelanggan/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="pelanggan/js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->	
	
	<!-- Banner Responsive slider -->
	<script src="pelanggan/js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
			// Slideshow 3
			$("#slider3").responsiveSlides({
				auto: true,
				pager: false,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});
	</script>
	<!-- // Banner Responsive slider -->
	
	<!-- stats -->
	<script src="pelanggan/js/jquery.waypoints.min.js"></script>
	<script src="pelanggan/js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	
	<!-- search-bar -->
	<script src="pelanggan/js/main.js"></script>
	<!-- //search-bar -->
	
	<!-- start-smoth-scrolling -->
	<script src="pelanggan/js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="pelanggan/js/move-top.js"></script>
	<script type="text/javascript" src="pelanggan/js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->

	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
	<script type="text/javascript" src="pelanggan/js/jquery-1.7.2.js"></script>
	<script src="pelanggan/js/jquery.quicksand.js" type="text/javascript"></script>
	<script src="pelanggan/js/script.js" type="text/javascript"></script>
	<script src="pelanggan/js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

<!-- //js-scripts -->

</body>
</html>