
<?php require_once("../koneksi.php");
    
    if (!isset($_SESSION)) {
        session_start();
    } ?><?php
	include "../koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Star Resto </title>
	
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Cafe In Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Meta tag Keywords -->
	
	<!-- css files -->
	<link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /> <!-- Style-CSS --> 
	<link rel="stylesheet" href="css/fontawesome-all.css"> <!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->

	<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />

	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Alegreya+Sans:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<!-- //web-fonts -->
	
</head>

<body>

<!-- Navigation -->
<header>
	<div class="top-nav">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand text-uppercase" href="index.html">Star Resto</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse justify-content-center pr-md-4" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link active" href="index.html">Menu <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="table_pesan.php"><img src="images/logo_ker.png" width="35"></a>
                                <span class="badge">
                                    <?php
					                if(isset($_SESSION['items'])){
					                  echo count($_SESSION['items']);
					                }
					                else{
					                  echo "0";
					                }
					                ?>
        						</span>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../index.php">Logout</a>
						</li>
						
						<!-- search --->
						<div class="search-bar-agileits">
							
								<!-- cd-header-buttons -->
							</div>
						</div>
						<!-- search --->
				
					</ul>
					
				</div>

			</nav>
		</div>
	</div>
</header>
<!-- //Navigation -->

<!-- //About us -->

<div class="clearfix"></div>
<br><br><br>
<!-- food gallery -->
<section class="banner_bottom proj py-5">
		<div class="wrap_view">
		<h3 class="heading text-center text-uppercase mb-5"> DAFTAR PEMESANAN </h3>
			<div class="inner_sec">

<form action="proses_pesan.php" method="post">
                            <div class="card-body">
                                <table id="" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <?php
                                                if (isset($_SESSION['items1'])) {
                                                    foreach ($_SESSION['items1'] as $key => $val) {
                                                        $query = mysqli_query($conn, "SELECT * FROM meja WHERE id_meja = '$key'");
                                                        $data = mysqli_fetch_array($query);
                                            ?>
                                                <input type="hidden" name="no_meja" value="<?php echo $data['id_meja']?>;">
                                            <?php
                                                    }
                                                }
                                            ?>
                                            <th>No</th>
                                            <th>Nama Masakan</th>
                                            <th>Harga</th>
                                            <th>Quantity</th>
                                            <th>Sub Total</th>
                                            <th>Keterangan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                  <tbody>
                                   <?php
                                    //MENAMPILKAN DETAIL KERANJANG BELANJA//
                                      $no = 1;
                                      $total = 0;
                                    //mysql_select_db($database_conn, $conn);
                                      if (isset($_SESSION['items'])) {
                                          foreach ($_SESSION['items'] as $key => $val) {
                                            $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
                                            $data = mysqli_fetch_array($query);
                                            $jumlah_barang = mysqli_num_rows($query);
                                            $jumlah_harga = $data['harga'] * $val;
                                            $total += $jumlah_harga;
                                            $harga = $data['harga'];
                                            $hasil = "Rp.".number_format($harga,2,',','.');
                                            $hasil1 = "Rp.".number_format($jumlah_harga,2,',','.');
                                            ?>
                                            <tr>
                                                <td><?php echo $no++; ?></td>
                                                <td><input type="hidden" name="id_masakan[]" value="<?php echo $data['id_masakan']; ?>"><?php echo $data['nama_masakan']; ?></td>
                                                <td><?php echo $hasil; ?></td>
                                                <td><a href="cart.php?act=plus&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=table_pesan.php" class="btn btn-default"><i class="ti-plus"></i>+</a><input type="hidden" name="jumlah[]" value="<?php echo ($val); ?>"><?php echo ($val); ?> Pcs <a href="cart.php?act=min&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=table_pesan.php" class="btn btn-default"><i class="ti-minus"></i>-</a>
                                                </td>
                                                <td><?php echo $hasil1; ?></td>
                                                <td><textarea class="form-control" name="keterangan[]"></textarea></td>
                                                <td><a href="cart.php?act=del&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=table_pesan.php">Cancel</a></td>
                                            </tr>
                                            <?php } } ?>                                      
                                            <?php
                                                if($total == 0){ ?>
                                                    <td colspan="4" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
                                                <?php } else { ?>
                                                <td colspan="6" style="font-size: 18px;"><b><div style="padding-right: 80px;" class="pull-right"><input type="hidden" value="<?php echo $total;?>" name="total_bayar">Total Harga Anda : Rp. <?php echo number_format($total); ?>,00 </div> </b></td>
          
                                            <?php 
                                            }
                                            ?>
                                  </tbody>
                                </table>
                                <p><div style="padding-right: 75px;" align="right">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModalCenter">&raquo; Pesan &laquo;</button>
                                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
									  <div class="modal-dialog modal-dialog-centered" role="document">
									    <div class="modal-content">
									      <div class="modal-header">
									        <h5 class="modal-title text-uppercase" id="exampleModalLongTitle">Pilih Meja</h5>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>
									      </div>
									      <div class="modal-body">
									      	<div class="form-group">
			                                    <select name="no_meja" class="form-control col-md-12">
			                                    	<option>-Pilih Meja-</option>
			                                    <?php     
			                                    include"../koneksi.php";
			                                      $query= $conn->query("SELECT * FROM meja WHERE status_meja = 'kosong'");
			                                      $hitung = mysqli_num_rows($query);
			                                      while($show=mysqli_fetch_array($query)){
			                                    ?>
			                                      	<option value="<?php echo $show['id_meja'];?>" ><?php echo $show['no_meja'];?></option>
			                                   <?php } ?>
			                                  </select>
			                                </div>
									      </div>
									      <div class="modal-footer">
									      	<?php
									      	if ($hitung < 1){
									      		echo"<div class='alert alert-danger' align='center'> Meja Sedang Penuh</div>";
									      	}else{
									      	?>
									      	<?php } ?>
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									        <button type="submit" class="btn btn-primary">Konfirmasi Pemesanan</button>
									      </div>
									    </div>
									  </div>
									</div>
                                    </div>
                                </p>
                            </div>
                        </form>
					<div class="clearfix"></div>
			</div>

		</div>
</section>
<!--//food gallery-->

<!-- footer -->	
<footer>
	<div class="container py-3 py-md-4">
		<div class="row">
			<div class="col-lg-5 col-md-12">
				<p class="py-lg-4">© 2019 UjikomTableService | Design by <a href="http://www.W3Layouts.com" target="_blank">W3Layouts</a></p>
			</div>
			<div class="col-lg-8 col-md-12">
				<a href="index.php">Star Resto</a>
			</div>
	</div>
</footer>
<!-- footer -->



<!-- js-scripts -->		

	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->	
	
	<!-- Banner Responsive slider -->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
			// Slideshow 3
			$("#slider3").responsiveSlides({
				auto: true,
				pager: false,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});
	</script>
	<!-- // Banner Responsive slider -->
	
	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	
	<!-- search-bar -->
	<script src="js/main.js"></script>
	<!-- //search-bar -->
	
	<!-- start-smoth-scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->

	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
	<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
	<script src="js/jquery.quicksand.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

<!-- //js-scripts -->

</body>
</html>