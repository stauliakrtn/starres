<?php
include '../koneksi.php';
require('../admin/fpdf/fpdf.php');

$pdf = new FPDF("P","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('../logo.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'Star Resto',0,'P');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 083811584860',0,'P');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Jl. Mangga',0,'P');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : starresto.sknc.site : staulkrtn21@gmail.com',0,'P');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(15.5,0.7,"Laporan Data Masakan",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(3.5, 0.8, 'No', 1, 0, 'C');
$pdf->Cell(3.5, 0.8, 'ID Masakan', 1, 0, 'C');
$pdf->Cell(4.5, 0.8, 'Nama Masakan', 1, 0, 'C');
$pdf->Cell(4.5, 0.8, 'Harga', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;


$query=mysqli_query($conn,"SELECT * FROM masakan order by id_masakan ASC");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(3.5, 0.6, $no , 1, 0, 'C');
	$pdf->Cell(3.5, 0.6, $lihat['id_masakan'],1, 0, 'C');
	$pdf->Cell(4.5, 0.6, $lihat['nama_masakan'], 1, 0,'C');
	$pdf->Cell(4.5, 0.6, $lihat['harga'],1, 1, 'C');

	$no++;
}
 
$pdf->Output("laporan_data_masakan.pdf","I");

?>

