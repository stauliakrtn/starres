-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2019 at 03:17 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL,
  `id_order` int(15) NOT NULL,
  `id_masakan` int(15) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_detail_order` varchar(30) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `jumlah`, `keterangan`, `status_detail_order`) VALUES
(162, 112, 22, 1, 'auu', 'Y'),
(163, 112, 26, 1, 's', 'Y'),
(164, 112, 24, 1, 'x', 'Y'),
(165, 112, 25, 1, 'n', 'Y'),
(166, 112, 22, 1, '', 'Y'),
(167, 112, 22, 1, '', 'Y'),
(168, 113, 25, 1, 'f', 'Menunggu'),
(169, 113, 26, 1, 'sad', 'Menunggu'),
(170, 113, 24, 1, 'ds', 'Menunggu'),
(171, 114, 22, 1, 'df', 'Y'),
(172, 114, 24, 1, 'f', 'Y'),
(173, 115, 24, 1, 'n', 'Y'),
(174, 116, 26, 1, 'nn', 'Menunggu'),
(175, 117, 25, 1, 'n', 'Y'),
(176, 118, 3, 1, 'dg', 'Menunggu');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(20) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'Administrator'),
(2, 'Waiter'),
(3, 'Kasir'),
(4, 'Owner'),
(5, 'Pelanggan');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE `masakan` (
  `id_masakan` int(11) NOT NULL,
  `nama_masakan` varchar(40) NOT NULL,
  `gambar` varchar(600) NOT NULL,
  `harga` int(20) NOT NULL,
  `status_makanan` varchar(2) NOT NULL DEFAULT '' COMMENT 'N ''Habis'' Y ''Tersedia'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `nama_masakan`, `gambar`, `harga`, `status_makanan`) VALUES
(1, 'Ayam Bakar', 'ayam bak.jpg', 50000, 'Y'),
(2, 'Ayam Goreng', 'ayam goreng.jpg', 40000, 'Y'),
(3, 'Bawal Bakar', 'bawal bakar.jpg', 55000, 'Y'),
(4, 'Bebek Bakar', 'bebek bakar.jpg', 50000, 'Y'),
(5, 'Bebek Goreng', 'bebek goreng.jpg', 53000, 'Y'),
(6, 'Cumi Goreng', 'cumi goreng.jpg', 30000, 'Y'),
(7, 'Empal Gepuk', 'empal.jpg', 50000, 'Y'),
(8, 'Ikan Gurame Bakar', 'ikan bakar.jpg', 45000, 'Y'),
(9, 'Kangkung', 'kangkung.jpg', 15000, 'Y'),
(10, 'Rendang', 'rendang.jpg', 55000, 'Y'),
(11, 'Kepiting Saos', 'kepiting.jpg', 50000, 'Y'),
(12, 'Nasi Goreng', 'nasi goreng.jpg', 30000, 'Y'),
(13, 'Opor Ayam', 'opor.jpg', 30000, 'Y'),
(14, 'Sate Ayam', 'sate.jpg', 35000, 'Y'),
(15, 'Nasi Timbel ', 'timbel.jpg', 55000, 'Y'),
(16, 'Jus Jeruk', 'jus jeruk.jpg', 15000, 'Y'),
(17, 'Ice Cappucino', 'ice cappucino.jpg', 15000, 'Y'),
(18, 'Jus Alpukat', 'jus alpukat.jpg', 15000, 'Y'),
(19, 'Jus Semangka', 'jus semangka.jpg', 15000, 'Y'),
(20, 'Ice Coffee Mocca', 'es Kopi moca.jpg', 20000, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE `meja` (
  `id_meja` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `status_meja` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`id_meja`, `no_meja`, `status_meja`) VALUES
(1, 1, 'kosong'),
(2, 2, 'kosong'),
(3, 3, 'kosong'),
(4, 4, 'kosong'),
(5, 5, 'penuh'),
(6, 6, 'penuh');

-- --------------------------------------------------------

--
-- Table structure for table `oder`
--

CREATE TABLE `oder` (
  `id_order` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status_order` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oder`
--

INSERT INTO `oder` (`id_order`, `no_meja`, `tanggal`, `id_user`, `keterangan`, `status_order`) VALUES
(112, 2, '2019-04-01', 22, '', 'Y'),
(113, 3, '2019-04-01', 28, '', 'Y'),
(114, 1, '2019-04-01', 8, '', 'Y'),
(115, 3, '2019-04-01', 8, '', 'Y'),
(116, 1, '2019-04-02', 21, '', 'Y'),
(117, 4, '2019-04-02', 27, '', 'Y'),
(118, 5, '2019-04-03', 21, '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `recovery_keys`
--

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recovery_keys`
--

INSERT INTO `recovery_keys` (`rid`, `userID`, `token`, `valid`) VALUES
(1, 1, '4e95261020dbeb2f9f1ea595536a9975', 1),
(2, 1, 'fd9bbf1c5df43b94c838099742012ab5', 1),
(3, 1, '41baee1c6135aba80561d50ef473fff4', 0),
(4, 1, '28f6c086571685749d4bbba311f3a017', 0),
(5, 1, 'f603482655be4ab2a9e59e9dc278cad9', 0),
(6, 1, '015b2cfefab75e50cccb047796dff941', 1),
(7, 9, '70a69b3706dedbb691964102f57254c2', 1),
(8, 9, 'e7f7f17d565a9aa2f41a83ac7711b319', 1),
(9, 9, '8227f62588ac2a1949ff69322d497364', 1),
(10, 9, '145e274a2375cc5700bcc6680084fffc', 0),
(11, 9, '24a5c2c4a4bd64f22ea8cd2b935c1343', 1),
(12, 9, 'd0294a0b58dfae672045749f4fc7d543', 1),
(13, 9, '503242c5e92055a284f415af55f5ed5d', 1),
(14, 9, '9142fb0d6d0c22be79fb9c6e01686b96', 0),
(15, 9, 'b220612b9d697cd109d279787656ee17', 0),
(16, 9, 'fc3b5216b8853e04511758331be6eb3e', 1),
(17, 9, '372affc99619c6beca6925df421c5a93', 0),
(18, 9, '74304d829e675f7dce103bbc23950b13', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(15) NOT NULL,
  `id_user` int(15) NOT NULL,
  `id_order` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(30) NOT NULL,
  `keterangan_transaksi` varchar(10) NOT NULL,
  `jumlah_uang` int(15) NOT NULL,
  `kembalian` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`, `keterangan_transaksi`, `jumlah_uang`, `kembalian`) VALUES
(101, 8, 115, '2019-04-01', 15000, 'Y', 50000, 35000),
(102, 25, 116, '2019-04-02', 20000, 'Y', 50000, 30000),
(103, 25, 117, '2019-04-02', 35000, 'Y', 50000, 15000),
(104, 0, 118, '0000-00-00', 55000, 'N', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `id_level` int(12) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif ,Y Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `email`, `nama_user`, `id_level`, `status`) VALUES
(8, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'pesek_hera@gmail.com', 'aulia', 1, 'Y'),
(21, 'meja1', '10705f86b703823d889c434c01419350', 'sitiauliak21@gmail.com', 'Meja 1', 5, 'Y'),
(22, 'waiter1', '27fa0d15b06cfc0f573d11b49c7e8414', 'sitiauliak21@gmail.com', 'Yuni', 2, 'Y'),
(23, 'pelanggan2', '81dc9bdb52d04dc20036dbd8313ed055', 'sitiauliak21@gmail.com', 'Meja 2', 5, 'Y'),
(24, 'meja4', '16d8fbd62375c5a77962ffd96c9275a2', 'sitiauliak21@gmail.com', 'Meja 4', 5, 'Y'),
(25, 'kasir1', '29c748d4d8f4bd5cbc0f3f60cb7ed3d0', 'sitiauliak21@gmail.com', 'Kasir1', 3, 'Y'),
(26, 'owner', '72122ce96bfec66e2396d2e25225d70a', 'staulkrtn21@gmail.com', 'Pemilik', 4, 'Y'),
(27, 'waiter', 'f64cff138020a2060a9817272f563b3c', 'jasaseobogor@gmal.com', 'Aulia', 2, 'Y'),
(28, 'aulia', '614913bc360cdfd1c56758cb87eb9e8f', 'sitiauliak21@gmail.com', 'Aulia Kartini', 5, 'Y'),
(29, 'vb', '35f4a8d465e6e1edc05f3d8ab658c551', 'bv', 'aulia kartini', 5, 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id_detail_order`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_masakan` (`id_masakan`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `masakan`
--
ALTER TABLE `masakan`
  ADD PRIMARY KEY (`id_masakan`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `oder`
--
ALTER TABLE `oder`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id_detail_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `masakan`
--
ALTER TABLE `masakan`
  MODIFY `id_masakan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
  MODIFY `id_meja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oder`
--
ALTER TABLE `oder`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
